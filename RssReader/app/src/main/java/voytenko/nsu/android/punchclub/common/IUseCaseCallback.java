package voytenko.nsu.android.punchclub.common;

public interface IUseCaseCallback<T> {
    void onSuccess(T response);
    void onError();
}
