package voytenko.nsu.android.punchclub.common;

import android.util.Log;

public final class UseCaseCallbackWrapper<T extends UseCase.ResponseValues> implements IUseCaseCallback<T> {
    @Override
    public void onSuccess(T response) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseCallbackWrapper.onSuccess");
        useCaseHandler.notifyResponse(response, callback);
    }

    @Override
    public void onError() {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseCallbackWrapper.onError");
        useCaseHandler.notifyError(callback);
    }

    UseCaseCallbackWrapper(IUseCaseCallback<T> callback, UseCaseHandler useCaseHandler){
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseCallbackWrapper.init");
        this.callback = callback;
        this.useCaseHandler = useCaseHandler;
    }

    private final IUseCaseCallback<T> callback;
    private final UseCaseHandler useCaseHandler;
}
