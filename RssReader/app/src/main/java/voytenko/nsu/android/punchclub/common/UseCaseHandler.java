package voytenko.nsu.android.punchclub.common;

import android.util.Log;

public class UseCaseHandler {


    public static UseCaseHandler getInstance(){
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseHandler.getInstance");
        if (sharedInstance == null){
            Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseHandler.init");
            sharedInstance = new UseCaseHandler(new UseCaseThreadPoolScheduler());
        }
        return sharedInstance;
    }

    public <T extends UseCase.RequestValues, R extends UseCase.ResponseValues>
    void execute(final UseCase<T, R> useCase, T requestValues, IUseCaseCallback<R> callback){
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseHandler.execute");

        useCase.setRequestValues(requestValues);
        useCase.setUseCaseCallback(new UseCaseCallbackWrapper<>(callback, this));

        useCaseScheduler.execute(new Runnable() {
            @Override
            public void run() {
                useCase.run();
            }
        });
    }

    <T extends UseCase.ResponseValues> void notifyResponse(T response, IUseCaseCallback<T> useCaseCallback){
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseHandler.notifyResponse");
        useCaseScheduler.onResponse(response, useCaseCallback);
    }

    <T extends UseCase.ResponseValues> void notifyError(IUseCaseCallback<T> useCaseCallback){
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseHandler.notifyError");
        useCaseScheduler.onError(useCaseCallback);
    }

    private UseCaseHandler(IUseCaseScheduler scheduler) {
        this.useCaseScheduler = scheduler;
    }

    private static UseCaseHandler sharedInstance;
    private final IUseCaseScheduler useCaseScheduler;
}
