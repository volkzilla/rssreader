package voytenko.nsu.android.punchclub.common;

import android.os.Handler;
import android.util.Log;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class UseCaseThreadPoolScheduler implements IUseCaseScheduler {

    UseCaseThreadPoolScheduler() {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseThreadPoolScheduler.init");
        this.threadPoolExecutor = new ThreadPoolExecutor(POOL_SIZE, MAX_POOL_SIZE, TIME_OUT, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(POOL_SIZE));
    }

    @Override
    public void execute(Runnable runnable) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseThreadPoolScheduler.execute");
        threadPoolExecutor.execute(runnable);
    }

    @Override
    public <T extends UseCase.ResponseValues> void onResponse(final T response, final IUseCaseCallback<T> callback) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseThreadPoolScheduler.onResponse");
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseThreadPoolScheduler.onResponse.run");
                callback.onSuccess(response);
            }
        });
    }

    @Override
    public <T extends UseCase.ResponseValues> void onError(final IUseCaseCallback<T> callback) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseThreadPoolScheduler.onError");
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(Constants.STACK_TRACE_LOG_TAG, "UseCaseThreadPoolScheduler.onError.run");
                callback.onError();
            }
        });
    }

    private static final int POOL_SIZE = 2;
    private static final int MAX_POOL_SIZE = 2;
    private static final int TIME_OUT = 2;

    private final ThreadPoolExecutor threadPoolExecutor;
    private final Handler handler = new Handler();
}
