package voytenko.nsu.android.punchclub.data.articles;

import android.util.Log;

import java.util.List;
import java.util.UUID;

import voytenko.nsu.android.punchclub.common.Constants;
import voytenko.nsu.android.punchclub.model.Post;

public class ArticlesFakeDataSource implements IArticlesDataSource {
    @Override
    public void getArticles(ILoadArticlesCallback callback) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "ArticlesFakeDataSource.getArticles");
        callback.onArticlesLoaded(posts);
    }

    @Override
    public void getArticle(final UUID articleId, ILoadArticleCallback callback) {
        Post post = posts.stream()
                .filter(a -> a.getUuid().compareTo(articleId) == 0)
                .findFirst()
                .get();

        callback.onArticleLoaded(post);
    }

    public static ArticlesFakeDataSource getInstance(){
        if (sharedInstance == null){
            sharedInstance = new ArticlesFakeDataSource();
        }
        return sharedInstance;
    }

    private ArticlesFakeDataSource() { }

    private static ArticlesFakeDataSource sharedInstance;

    private List<Post> posts = FakeNewsStore.articles();
}
