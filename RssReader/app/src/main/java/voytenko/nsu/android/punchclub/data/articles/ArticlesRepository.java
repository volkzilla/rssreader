package voytenko.nsu.android.punchclub.data.articles;

import android.util.Log;

import java.util.UUID;

import voytenko.nsu.android.punchclub.common.Constants;

public class ArticlesRepository implements IArticlesDataSource {

    public ArticlesRepository(IArticlesDataSource articlesFakeDataSource) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "ArticlesRepository.init");
        this.articlesFakeDataSource = articlesFakeDataSource;
    }

    @Override
    public void getArticles(ILoadArticlesCallback callback) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "ArticlesRepository.getArticles");
        if (callback != null){
            articlesFakeDataSource.getArticles(callback);
        }
    }

    @Override
    public void getArticle(UUID articleId, ILoadArticleCallback callback) {
        if (callback != null){
            articlesFakeDataSource.getArticle(articleId, callback);
        }
    }

    private IArticlesDataSource articlesFakeDataSource;
}
