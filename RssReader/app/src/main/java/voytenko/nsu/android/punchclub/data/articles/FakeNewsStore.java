package voytenko.nsu.android.punchclub.data.articles;

import java.util.ArrayList;
import java.util.Date;

import voytenko.nsu.android.punchclub.R;
import voytenko.nsu.android.punchclub.model.Post;
import voytenko.nsu.android.punchclub.model.Channel;

public class FakeNewsStore {

    public static ArrayList<Post> articles(){
        Channel channel1 = new Channel();
        channel1.setName("Main channel");
        channel1.setCreationDate(new Date());
        channel1.setLinkString("localhost");

        Post post1 = new Post();
        post1.setChannelUuid(channel1.getUuid());
        post1.setTitle("Хочу подраться на технику");
        post1.setSummary("Привет всем техничным бойцам клуба...");
        post1.setFullText("Всем привет! Хочу подраться с кем-нибудь, у кого хорошо поставлена техника ударов, болевый и так далее"
                + "\n" +
                "Мне нужно научиться бить и защищаться правильно. Надеюсь, умельцы найдутся быстро!!");
        post1.setPublicationDate(new Date());
        post1.setImageLink(R.drawable.a);

        Post post2 = new Post();
        post2.setChannelUuid(channel1.getUuid());
        post2.setTitle("Пацаны, надо срочно выпустить гнев!");
        post2.setSummary("После работы вечерком....");
        post2.setFullText("Чуваки, я тут короче задолбался на этой гребаной работе тухнуть, решил вечерком набить кому-нибудь морду. \n" +
                "Надо ведь как-то гнев выпускать! Если все нормально у нас пойдет, то и, это, закорефанимся мб.\n" +
                "В качестве бонуса могу угостить напарника пивом после дуэли!");
        post2.setPublicationDate(new Date());
        post2.setImageLink(R.drawable.b);

        Post post3 = new Post();
        post3.setChannelUuid(channel1.getUuid());
        post3.setTitle("Я старый, но порох еще есть");
        post3.setSummary("Давайте устроим культурный бой");
        post3.setFullText("Гражданин в годах ищет себе напарника на культурный и честный бой. Предлагаю подраться просто на кулаках,\n" +
                "но с намотанными бинтами - аккуратность привыше всего, как никак! По времени думаю все займет минут 40,\n" +
                "не больше. Если вы заинтересованы, наберите мне после 20-00 (а то жена может увидать мой мобильник, мда)\n" +
                "на следующий номер - 8-915-777-44-31. Зовут Тимофей Эдуардович (можно просто Тимофей, но лучше Эдуардович).");
        post3.setPublicationDate(new Date());
        post3.setImageLink(R.drawable.c);

        Post post4 = new Post();
        post4.setChannelUuid(channel1.getUuid());
        post4.setTitle("Хочу подраться на технику");
        post4.setSummary("Привет всем техничным бойцам клуба...");
        post4.setFullText("Всем привет! Хочу подраться с кем-нибудь, у кого хорошо поставлена техника ударов, болевый и так далее"
                + "\n" +
                "Мне нужно научиться бить и защищаться правильно. Надеюсь, умельцы найдутся быстро!!");
        post4.setPublicationDate(new Date());
        post4.setImageLink(R.drawable.a);

        Post post5 = new Post();
        post5.setChannelUuid(channel1.getUuid());
        post5.setTitle("Пацаны, надо срочно выпустить гнев!");
        post5.setSummary("После работы вечерком....");
        post5.setFullText("Чуваки, я тут короче задолбался на этой гребаной работе тухнуть, решил вечерком набить кому-нибудь морду. \n" +
                "Надо ведь как-то гнев выпускать! Если все нормально у нас пойдет, то и, это, закорефанимся мб.\n" +
                "В качестве бонуса могу угостить напарника пивом после дуэли!");
        post5.setPublicationDate(new Date());
        post5.setImageLink(R.drawable.b);

        Post post6 = new Post();
        post6.setChannelUuid(channel1.getUuid());
        post6.setTitle("Я старый, но порох еще есть");
        post6.setSummary("Давайте устроим культурный бой");
        post6.setFullText("Гражданин в годах ищет себе напарника на культурный и честный бой. Предлагаю подраться просто на кулаках,\n" +
                "но с намотанными бинтами - аккуратность привыше всего, как никак! По времени думаю все займет минут 40,\n" +
                "не больше. Если вы заинтересованы, наберите мне после 20-00 (а то жена может увидать мой мобильник, мда)\n" +
                "на следующий номер - 8-915-777-44-31. Зовут Тимофей Эдуардович (можно просто Тимофей, но лучше Эдуардович).");
        post6.setPublicationDate(new Date());
        post6.setImageLink(R.drawable.c);

        Post post7 = new Post();
        post7.setChannelUuid(channel1.getUuid());
        post7.setTitle("Хочу подраться на технику");
        post7.setSummary("Привет всем техничным бойцам клуба...");
        post7.setFullText("Всем привет! Хочу подраться с кем-нибудь, у кого хорошо поставлена техника ударов, болевый и так далее"
                + "\n" +
                "Мне нужно научиться бить и защищаться правильно. Надеюсь, умельцы найдутся быстро!!");
        post7.setPublicationDate(new Date());
        post7.setImageLink(R.drawable.a);

        Post post8 = new Post();
        post8.setChannelUuid(channel1.getUuid());
        post8.setTitle("Пацаны, надо срочно выпустить гнев!");
        post8.setSummary("После работы вечерком....");
        post8.setFullText("Чуваки, я тут короче задолбался на этой гребаной работе тухнуть, решил вечерком набить кому-нибудь морду. \n" +
                "Надо ведь как-то гнев выпускать! Если все нормально у нас пойдет, то и, это, закорефанимся мб.\n" +
                "В качестве бонуса могу угостить напарника пивом после дуэли!");
        post8.setPublicationDate(new Date());
        post8.setImageLink(R.drawable.b);

        Post post9 = new Post();
        post9.setChannelUuid(channel1.getUuid());
        post9.setTitle("Я старый, но порох еще есть");
        post9.setSummary("Давайте устроим культурный бой");
        post9.setFullText("Гражданин в годах ищет себе напарника на культурный и честный бой. Предлагаю подраться просто на кулаках,\n" +
                "но с намотанными бинтами - аккуратность привыше всего, как никак! По времени думаю все займет минут 40,\n" +
                "не больше. Если вы заинтересованы, наберите мне после 20-00 (а то жена может увидать мой мобильник, мда)\n" +
                "на следующий номер - 8-915-777-44-31. Зовут Тимофей Эдуардович (можно просто Тимофей, но лучше Эдуардович).");
        post9.setPublicationDate(new Date());
        post9.setImageLink(R.drawable.c);

        Post post10 = new Post();
        post10.setChannelUuid(channel1.getUuid());
        post10.setTitle("Я старый, но порох еще есть");
        post10.setSummary("Давайте устроим культурный бой");
        post10.setFullText("Гражданин в годах ищет себе напарника на культурный и честный бой. Предлагаю подраться просто на кулаках,\n" +
                "но с намотанными бинтами - аккуратность привыше всего, как никак! По времени думаю все займет минут 40,\n" +
                "не больше. Если вы заинтересованы, наберите мне после 20-00 (а то жена может увидать мой мобильник, мда)\n" +
                "на следующий номер - 8-915-777-44-31. Зовут Тимофей Эдуардович (можно просто Тимофей, но лучше Эдуардович).");
        post10.setPublicationDate(new Date());
        post10.setImageLink(R.drawable.a);

        ArrayList<Post> posts = new ArrayList<>();
        posts.add(post1);
        posts.add(post2);
        posts.add(post3);
        posts.add(post4);
        posts.add(post5);
        posts.add(post6);
        posts.add(post7);
        posts.add(post8);
        posts.add(post9);
        posts.add(post10);

        return posts;
    }
}
