package voytenko.nsu.android.punchclub.data.articles;

import java.util.List;
import java.util.UUID;

import voytenko.nsu.android.punchclub.model.Post;

public interface IArticlesDataSource {

    interface ILoadArticlesCallback{
        void onArticlesLoaded(List<Post> posts);
    }

    interface ILoadArticleCallback{
        void onArticleLoaded(Post post);
    }

    void getArticles(ILoadArticlesCallback callback);

    void getArticle(UUID articleId, ILoadArticleCallback callback);
}
