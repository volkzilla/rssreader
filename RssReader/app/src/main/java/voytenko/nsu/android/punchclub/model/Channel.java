package voytenko.nsu.android.punchclub.model;

import java.util.Date;
import java.util.UUID;

public class Channel {

    public Channel(){
        this.uuid = UUID.randomUUID();
    }

    public Channel(String name, String linkString, Date creationDate) {
        this.uuid = UUID.randomUUID();
        this.creationDate = new Date();
        this.name = name;
        this.linkString = linkString;
        this.creationDate = creationDate;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getLinkString() {
        return linkString;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLinkString(String linkString) {
        this.linkString = linkString;
    }

    private UUID uuid;
    private String name;
    private String linkString;
    private Date creationDate;
}
