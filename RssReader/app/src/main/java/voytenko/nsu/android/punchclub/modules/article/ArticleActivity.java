package voytenko.nsu.android.punchclub.modules.article;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import java.util.UUID;

import voytenko.nsu.android.punchclub.R;
import voytenko.nsu.android.punchclub.common.UseCaseHandler;
import voytenko.nsu.android.punchclub.data.articles.ArticlesFakeDataSource;
import voytenko.nsu.android.punchclub.data.articles.ArticlesRepository;

public class ArticleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        String articleString = getIntent().getStringExtra(EXTRA_ARTICLE_ID);
        UUID artcleId = UUID.fromString(articleString);

        FragmentManager fragmentManager = getSupportFragmentManager();
        ArticleFragment articleFragment = (ArticleFragment) fragmentManager.findFragmentById(R.id.fragment_container);

        if (articleFragment == null){
            articleFragment = ArticleFragment.newInstance();
            fragmentManager
                    .beginTransaction()
                    .add(R.id.fragment_container, articleFragment)
                    .commit();
        }


        UseCaseHandler useCaseHandler = UseCaseHandler.getInstance();

        ArticlesFakeDataSource articlesFakeDataSource = ArticlesFakeDataSource.getInstance();
        ArticlesRepository articlesRepository = new ArticlesRepository(articlesFakeDataSource);
        GetArticle getArticle = new GetArticle(articlesRepository);

        IArticlePresenter presenter = new ArticlePresenter(articleFragment, useCaseHandler, artcleId, getArticle);
        articleFragment.setPresenter(presenter);
    }

    public static final String EXTRA_ARTICLE_ID = "EXTRA_ARTICLE_ID";
}
