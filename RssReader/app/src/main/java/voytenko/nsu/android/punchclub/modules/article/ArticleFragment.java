package voytenko.nsu.android.punchclub.modules.article;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import voytenko.nsu.android.punchclub.R;
import voytenko.nsu.android.punchclub.model.Post;

public class ArticleFragment extends Fragment implements IArticleView{

    public static ArticleFragment newInstance(){
        return new ArticleFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        configureView(view);
        return view;
    }

    private void configureView(View view){
        articleTitleTextView = view.findViewById(R.id.article_title);
        articleImageView = view.findViewById(R.id.article_image);
        articlePubDateTextView = view.findViewById(R.id.article_publication_date);
        articleFullTextView = view.findViewById(R.id.article_full_text);
    }

    @Override
    public void setTitle(String title) {
        this.articleTitleTextView.setText(title);
    }

    @Override
    public void setFullText(String fullText) {
        articleFullTextView.setText(fullText);
    }

    @Override
    public void setImage(int imageId) {
        this.articleImageView.setImageResource(imageId);
    }

    @Override
    public void setDate(String date) {
        Date currentDate = new Date();
        this.articlePubDateTextView.setText(currentDate.toString());
    }

    @Override
    public void setPresenter(IArticlePresenter presenter) {
        this.presenter = presenter;
    }

    private TextView articleTitleTextView;
    private ImageView articleImageView;
    private TextView articlePubDateTextView;
    private TextView articleFullTextView;

    private Post post;

    private IArticlePresenter presenter;
}
