package voytenko.nsu.android.punchclub.modules.article;


import android.util.Log;

import java.util.UUID;

import voytenko.nsu.android.punchclub.common.IUseCaseCallback;
import voytenko.nsu.android.punchclub.common.UseCaseHandler;
import voytenko.nsu.android.punchclub.model.Post;

public class ArticlePresenter implements IArticlePresenter {

    public ArticlePresenter(final IArticleView view, final UseCaseHandler useCaseHandler, final UUID artcleId, final GetArticle getArticle){
        this.view = view;
        this.useCaseHandler = useCaseHandler;
        this.articleId = artcleId;
        this.getArticle = getArticle;
    }

    @Override
    public void start() {
        loadArticle();
    }

    private void loadArticle(){
        GetArticle.RequestValues requestValues = new GetArticle.RequestValues(articleId);
        useCaseHandler.execute(getArticle, requestValues, new IUseCaseCallback<GetArticle.ResponseValues>() {
            @Override
            public void onSuccess(GetArticle.ResponseValues response) {
                Post post = response.getPost();
                proccessArticle(post);
            }

            @Override
            public void onError() {

            }
        });
    }

    private void proccessArticle(Post post){
        if (post == null){
            Log.d("ArticlePresenter", "Catch null post.");
            return;
        }
        String title = post.getTitle();
        String fullText = post.getFullText();
        int imageId = post.getImageLink();
        String date = post.getPublicationDate().toString();

        if (title != null){
            view.setTitle(title);
        }

        if (fullText != null){
            view.setFullText(fullText);
        }

        if (imageId != 0){
            view.setImage(imageId);
        }

        view.setDate(date);
    }

    private final IArticleView view;
    private final UseCaseHandler useCaseHandler;
    private final GetArticle getArticle;
    private final UUID articleId;
}
