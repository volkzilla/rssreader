package voytenko.nsu.android.punchclub.modules.article;

import android.util.Log;

import java.util.UUID;

import voytenko.nsu.android.punchclub.common.Constants;
import voytenko.nsu.android.punchclub.common.UseCase;
import voytenko.nsu.android.punchclub.data.articles.ArticlesRepository;
import voytenko.nsu.android.punchclub.model.Post;

public class GetArticle extends UseCase<GetArticle.RequestValues, GetArticle.ResponseValues> {

    GetArticle(ArticlesRepository articlesRepository){
        Log.d(Constants.STACK_TRACE_LOG_TAG, "GetArticles.init");
        this.articlesRepository = articlesRepository;
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "GetArticles.executeUseCase");

        UUID articleId = requestValues.getArticleId();
        articlesRepository.getArticle(articleId, article -> {
            ResponseValues responseValues = new ResponseValues(article);
            getUseCaseCallback().onSuccess(responseValues);
        });
    }

    public static final class RequestValues implements UseCase.RequestValues{
        private UUID articleId;

        RequestValues(UUID articleId) {
            this.articleId = articleId;
        }

        public UUID getArticleId() {
            return articleId;
        }
    }

    static final class ResponseValues implements UseCase.ResponseValues{

        private final Post post;

        ResponseValues(Post post) {
            this.post = post;
        }

        Post getPost() {
            return post;
        }
    }

    private final ArticlesRepository articlesRepository;
}