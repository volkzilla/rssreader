package voytenko.nsu.android.punchclub.modules.article;

import voytenko.nsu.android.punchclub.modules.base.IBaseView;

interface IArticleView extends IBaseView<IArticlePresenter> {
    void setTitle(String title);
    void setFullText(String fullText);
    void setImage(int imageId);
    void setDate(String date);
}
