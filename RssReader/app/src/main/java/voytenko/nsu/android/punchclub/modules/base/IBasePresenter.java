package voytenko.nsu.android.punchclub.modules.base;

public interface IBasePresenter {
    void start();
}
