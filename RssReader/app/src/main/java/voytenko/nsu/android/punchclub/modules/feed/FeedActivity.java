package voytenko.nsu.android.punchclub.modules.feed;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import voytenko.nsu.android.punchclub.R;
import voytenko.nsu.android.punchclub.common.Constants;
import voytenko.nsu.android.punchclub.common.UseCaseHandler;
import voytenko.nsu.android.punchclub.data.articles.ArticlesFakeDataSource;
import voytenko.nsu.android.punchclub.data.articles.ArticlesRepository;

public class FeedActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "FeedActivity.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FeedFragment feedFragment = (FeedFragment) fragmentManager.findFragmentById(R.id.fragment_container);

        if (feedFragment == null){
            feedFragment = FeedFragment.newInstance();
            fragmentManager
                    .beginTransaction()
                    .add(R.id.fragment_container, feedFragment)
                    .commit();
        }

        UseCaseHandler useCaseHandler = UseCaseHandler.getInstance();
        ArticlesFakeDataSource articlesFakeDataSource = ArticlesFakeDataSource.getInstance();
        ArticlesRepository articlesRepository = new ArticlesRepository(articlesFakeDataSource);
        GetArticles getArticles = new GetArticles(articlesRepository);

        IFeedPresenter presenter = new FeedPresenter(feedFragment, getArticles, useCaseHandler);
        feedFragment.setPresenter(presenter);
    }

}
