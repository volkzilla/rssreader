package voytenko.nsu.android.punchclub.modules.feed;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import voytenko.nsu.android.punchclub.R;
import voytenko.nsu.android.punchclub.common.Constants;
import voytenko.nsu.android.punchclub.model.Post;
import voytenko.nsu.android.punchclub.modules.article.ArticleActivity;

public class FeedFragment extends Fragment implements IFeedView{


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "FeedFragment.onCreateView");
        View view = inflater.inflate(R.layout.fragment_feed, container, false);

        feedRecycleView = view.findViewById(R.id.feed_recycler_view);
        feedRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        presenter.start();
    }

    @Override
    public void showArticles(List<Post> postList) {
        articleAdapter = new ArticleAdapter(postList);
        feedRecycleView.setAdapter(articleAdapter);
    }

    @Override
    public void openArticleDetailScreen(String artickleId) {
        Log.d(ArticleActivity.EXTRA_ARTICLE_ID, artickleId);
        //it's show operation of Activity call
        Intent intent = new Intent(getContext(), ArticleActivity.class);
        intent.putExtra(ArticleActivity.EXTRA_ARTICLE_ID, artickleId);
        startActivity(intent);
    }

    @Override
    public void setPresenter(IFeedPresenter presenter) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "FeedFragment.setPresenter");
        this.presenter = presenter;
    }

    private class ArticleHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ArticleHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.feed_item, parent, false));

            itemView.setOnClickListener(this);

            titleTextView = itemView.findViewById(R.id.article_title);
            summaryTextView = itemView.findViewById(R.id.article_summary);
        }

        void bind(Post post){
            this.post = post;
            titleTextView.setText(post.getTitle());
            summaryTextView.setText(post.getSummary());
        }

        @Override
        public void onClick(View v) {
            presenter.tapOnArticle(post);
        }


        private Post post;
        private TextView titleTextView;
        private TextView summaryTextView;

    }

    private class ArticleAdapter extends RecyclerView.Adapter<ArticleHolder>{
        private List<Post> posts;

        ArticleAdapter(List<Post> posts){
            this.posts = posts;
        }

        @Override
        public ArticleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new ArticleHolder(inflater, parent);
        }

        @Override
        public void onBindViewHolder(ArticleHolder holder, int i) {
            Post post = posts.get(i);

            holder.bind(post);
        }

        @Override
        public int getItemCount() {
            return posts.size();
        }
    }

    private RecyclerView feedRecycleView;
    private ArticleAdapter articleAdapter;
    private IFeedPresenter presenter;

    public static FeedFragment newInstance(){
        return new FeedFragment();
    }
}
