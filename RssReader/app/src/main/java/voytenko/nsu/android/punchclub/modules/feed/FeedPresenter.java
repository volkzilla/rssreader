package voytenko.nsu.android.punchclub.modules.feed;

import android.util.Log;

import java.util.List;

import voytenko.nsu.android.punchclub.common.Constants;
import voytenko.nsu.android.punchclub.common.IUseCaseCallback;
import voytenko.nsu.android.punchclub.common.UseCaseHandler;
import voytenko.nsu.android.punchclub.model.Post;

public class FeedPresenter implements IFeedPresenter {

    FeedPresenter(IFeedView view, GetArticles getArticles, UseCaseHandler useCaseHandler){
        Log.d(Constants.STACK_TRACE_LOG_TAG, "FeedPresenter.init");
        this.view = view;
        this.getArticles = getArticles;
        this.useCaseHandler = useCaseHandler;
    }

    @Override
    public void start() {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "FeedPresenter.start");
        loadFeed();
    }

    @Override
    public void tapOnArticle(Post post) {
        view.openArticleDetailScreen(post.getUuid().toString());
    }

    private void loadFeed(){
        Log.d(Constants.STACK_TRACE_LOG_TAG, "FeedPresenter.loadFeed");
        final GetArticles.RequestValues requestValues = new GetArticles.RequestValues(true);

        useCaseHandler.execute(getArticles, requestValues, new IUseCaseCallback<GetArticles.ResponseValues>() {
            @Override
            public void onSuccess(GetArticles.ResponseValues response) {
                List<Post> posts = response.getPosts();
                proccessArticles(posts);
            }

            @Override
            public void onError() {

            }
        });
    }


    private void proccessArticles(List<Post> posts){
        view.showArticles(posts);
    }

    private GetArticles getArticles;
    private IFeedView view;
    private final UseCaseHandler useCaseHandler;

}
