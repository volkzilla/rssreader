package voytenko.nsu.android.punchclub.modules.feed;

import android.util.Log;

import java.util.List;

import voytenko.nsu.android.punchclub.common.Constants;
import voytenko.nsu.android.punchclub.common.UseCase;
import voytenko.nsu.android.punchclub.data.articles.ArticlesRepository;
import voytenko.nsu.android.punchclub.data.articles.IArticlesDataSource;
import voytenko.nsu.android.punchclub.model.Post;

public class GetArticles extends UseCase<GetArticles.RequestValues, GetArticles.ResponseValues> {

    GetArticles(ArticlesRepository articlesRepository){
        Log.d(Constants.STACK_TRACE_LOG_TAG, "GetArticles.init");
        this.articlesRepository = articlesRepository;
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {
        Log.d(Constants.STACK_TRACE_LOG_TAG, "GetArticles.executeUseCase");

        articlesRepository.getArticles(new IArticlesDataSource.ILoadArticlesCallback() {
            @Override
            public void onArticlesLoaded(List<Post> posts) {
                Log.d(Constants.STACK_TRACE_LOG_TAG, "GetArticles.executeUseCase.onArticlesLoaded");
                ResponseValues responseValues = new ResponseValues(posts);
                getUseCaseCallback().onSuccess(responseValues);
            }
        });
    }

    public static final class RequestValues implements UseCase.RequestValues{
        private boolean fetchFromFakeStore;


        RequestValues(boolean fetchFromFakeStore) {
            this.fetchFromFakeStore = fetchFromFakeStore;
        }

        public boolean isFetchFromFakeStore() {
            return fetchFromFakeStore;
        }
    }

    static final class ResponseValues implements UseCase.ResponseValues{

        private final List<Post> posts;

        ResponseValues(List<Post> posts) {
            this.posts = posts;
        }

        List<Post> getPosts() {
            return posts;
        }
    }

    private final ArticlesRepository articlesRepository;
}
