package voytenko.nsu.android.punchclub.modules.feed;

import voytenko.nsu.android.punchclub.model.Post;
import voytenko.nsu.android.punchclub.modules.base.IBasePresenter;

interface IFeedPresenter extends IBasePresenter {
    void tapOnArticle(Post post);
}
