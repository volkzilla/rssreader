package voytenko.nsu.android.punchclub.modules.feed;

import java.util.List;

import voytenko.nsu.android.punchclub.model.Post;
import voytenko.nsu.android.punchclub.modules.base.IBaseView;

public interface IFeedView extends IBaseView<IFeedPresenter> {

    void showArticles(List<Post> postList);

    void openArticleDetailScreen(String artickleId);

}
